# Full-stack Teacher/School application
A Full-stack React and React-router based application using RESTful server API backed by a PostgreSQL database.

[Repo URL at gitlab.com](https://gitlab.com/cval-c4q/fullstack-teacher-school)

## Code organization
This respository contains both front and back-end component, side by side.
* __index.js__: Spawns both the back-end and front-end services, on different ports
* __routes.js__: The back-end express Application, exported through this module
* __seed.sql__: Modified PostgreSQL db seed script.
* __static/\*__: This is the front-end code, served as static content, transpiled at runtime
  * ├ __index.html__: inlining scripts, it pulls in all dependencies and calls ReactRouterDOM.render
  * └ __com-\*.js__: code for the components/component stubs.

### Note on front-end routes
The _/teachers_ and _/teachers/search_ routes retrieve the bulk of the data from the back-end and do their own filtering and processing, while the other two routes delegate some of the filtering to the server routes (_POST /teachers/school_ and _POST /teachers/subject_).

The routes also tend to fetch and cache data per component state.
Refactoring to ensure data freshness should be trivial.

### Back-end dependencies
This is really stripped down to the basics and built from scratch: No react-create-app, no Webpack.
* [Expressjs/express](https://github.com/expressjs/express)
* [node-postgres (pg)](https://github.com/brianc/node-postgres)
* [Expressjs/cors](https://github.com/expressjs/cors)
* [Expressjs/morgan](https://github.com/expressjs/morgan)
* [Nodemon](https://github.com/remy/nodemon)
* [circular-json](https://github.com/WebReflection/circular-json)
* [Chalk](https://github.com/chalk/chalk)

## Deployment
```bash
psql -f seed.sql	# Resets seed SQL data
npm/yarn install
npm/yarn run test	# ESlint, optional
npm/yarn run start
```
