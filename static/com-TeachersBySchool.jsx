/***
 *  Component performs incremental search for Teachers by school name
 *  Displays sperate <ul>'s for each matched school.
 */

const QUERY_HOLDOFF = 762; // msec
let _holdoffTimer = undefined; // timer handle

class TeachersBySchool extends React.Component {
	constructor() {
		super();
		this.state  = {};
	}

	componentWillMount() {
		window.fetch("http://localhost:3001/schools", { method: "GET", cache: "no-store" })
			.then(res => res.json())
			.then(json => {
				this.setState( { schools: json } );
			});
	}

	// Functional helper
	teachersByExactSchoolName(sname) {
		const schools = this.state.schools.data.rows;
		const teachers = this.state.teachers.data.rows;
		return teachers.filter((teacher) => {
			return schools.find(sch => sch.id === teacher.schoolid).name === sname;
		});
	}

	render() {
		if (this.state.schools === undefined)
			return <h2>Fetching data from server...</h2>;
		else if (this.state.schools && this.state.schools.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve school data from back-end server.</h2><br />
					{ "Error: " + this.state.schools.error }<br />
					{ "Details: " + this.state.teachers.details }<br />
				</h3>
			);
		else if (this.state.teachers && this.state.teachers.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve teacher data from back-end server.</h2><br />
					{ "Error: " + this.state.teachers.error }<br />
					{ "Details: " + this.state.teachers.details }<br />
				</h3>
			);
		else
			return (
				<div className="Namesearch-list">
					<input type="text" id="searchBox" placeholder="Start typing school name..."
						onChange={this.doIncrementalSearch.bind(this)}/>
						{
						this.state.schools.data.rows.filter((school) => {
							if (!(this.state.partialName && this.state.partialName.trim().length > 0))
								return false;
							let re = new RegExp("^" + this.state.partialName.trim(), "i");
							return school.name.search(re) !== -1;
							}).map((school, schIdx) => {
							return (
								<ul key={schIdx} className="Teacher-list">
									<li>{school.name}</li>
									<li><ul>
											{
												this.teachersByExactSchoolName(school.name).map((teacher, teacherIdx) => <li key={teacherIdx}>{teacher.name}</li>)
											}
									</ul></li>
								</ul>
								);
							})
						}
					</div>
			);
	}

	doIncrementalSearch(ev) {
		const searchBoxVal = ev.target.value;
		if (_holdoffTimer) {
			clearTimeout(_holdoffTimer);
		}
		_holdoffTimer = setTimeout((function() {
			_holdoffTimer = undefined;
			if (searchBoxVal.length > 0 && searchBoxVal.trim().length > 0) {
				let reqBody = JSON.stringify({ name: searchBoxVal.trim() });
				let req = new Request("http://localhost:3001/teachers/school", { method: "POST", body: reqBody, cache: "no-store", headers: {"Content-Type":"application/json"}});
				window.fetch(req)
					.then(res => res.json())
					.then(json => {
						this.setState({ partialName: searchBoxVal.trim(), teachers: json });
					});
			}
		}).bind(this), QUERY_HOLDOFF);
	}
}

//window.Teachers.TeachersBySchool = TeachersBySchool;
// vi: ai sw=2 ts=2
