

class Teachers extends React.Component {
	componentWillMount() {
		window.fetch("http://localhost:3001/teachers/all/", { method: "GET" })
			.then(res => res.json())
			.then(json => {
				this.setState( { teachers: json } );
			}).catch(e => alert(e.toString()));

		window.fetch("http://localhost:3001/schools", { method: "GET" })
			.then(res => res.json())
			.then(json => {
				this.setState( { schools: json } );
			}).catch(e => {console.error(e); alert(e.toString());});
	}
	render() {
		if (!(this.state && this.state.teachers && this.state.schools))
			return <h2>Fetching teacher data...</h2>;
		else if (this.state.teachers.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve teacher data from back-end server.</h2><br />
					{ "Error: " + this.state.teachers.error }<br />
					{ "Details: " + this.state.teachers.details }<br />
				</h3>
			);
		else if (this.state.schools.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve school data from back-end server.</h2><br />
					{ "Error: " + this.state.schools.error }<br />
					{ "Details: " + this.state.schools.details }<br />
				</h3>
			);
		else {
			const schools = this.state.schools.data.rows;
			return (
			<div className="Teacher-list">
			<ul>
				{
				this.state.teachers.data.rows.map((teacher, idx) => <li key={idx}>
					{teacher.name} (ID #{teacher.id})<br />
					&nbsp;Subject: {teacher.subject}<br />
					&nbsp;School: {teacher.schoolid ? schools.find(school => school.id === teacher.schoolid).name : "N/A"}
				</li>)
				}
			</ul>
			</div>
		);
		}
	}
}

//window.Teachers.Teachers = Teachers;
// vi: ai ts=4 sw=4
