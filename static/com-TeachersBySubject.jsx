/**
 *  Teacher search by study Subject
 */

const QUERY_HOLDOFF = 762;	// msec
let _holdoffTimer = undefined; // timer handle

class TeachersBySubject extends React.Component {
	constructor() {
		super();
		this.state  = {};
	}

	componentWillMount() {
		/**
		 *  Construct a list of all possible subjects we might encounter */
		function uniqueSubjects(json) {
			let s = new Set();
			json.data.rows.forEach(teacher => s.add(teacher.subject));
			return Array.from(s);
		}
		window.fetch("http://localhost:3001/teachers/all", { method: "GET" })
			.then(res => res.json())
			.then(json => {
				if (json.success === false) {
					this.setState( { subjects: json } );
				} else {
					this.setState( { teachers: json, subjects: {
						success: true, data: uniqueSubjects(json) } } );
				}
			});
	}

	render() {
		const teachersByExactSubjectName = (sname) => {
			const teachers = this.state.teachers.data.rows;
			return teachers.filter(teacher => teacher.subject === sname);
		};

		if (this.state.subjects === undefined || this.state.teachers === undefined)
			return <h3>Fetching data from server...</h3>;
		else if (this.state.subjects && this.state.subjects.success === false || this.state.teachers && this.state.teachers.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve teacher data (to reconstitue subject list) from back-end server.</h2><br />
					{ "Error: " + this.state.teachers.error }<br />
					{ "Details: " + this.state.teachers.details }<br />
				</h3>
			);
		else
			return (
				<div className="Namesearch-list">
					<input type="text" id="searchBox" placeholder="Start typing school name..."
						onChange={this.doIncrementalSearch.bind(this)}/>
						{
						this.state.subjects.data.filter(subject => {
							if (!(this.state.partialName && this.state.partialName.trim().length > 0))
								return false;
							let re = new RegExp("^" + this.state.partialName.trim(), "i");
							return subject.search(re) !== -1;
						}).map((subject, subIdx) => {
							return (
								<ul key={subIdx} className="Teacher-list">
									<li>{subject}</li>
									<li><ul>
											{
												teachersByExactSubjectName(subject).map((teacher, teacherIdx) => <li key={teacherIdx}>{teacher.name}</li>)
											}
									</ul></li>
								</ul>
								);
							})
						}
					</div>
			);
	}

/**
 *  Relies on server back-end to do the filtering.
 *  Complete teacher list is still maintained in state for display formatting purposes.
 */
	doIncrementalSearch(ev) {
		const searchBoxVal = ev.target.value;
		if (_holdoffTimer) {
			clearTimeout(_holdoffTimer);
		}
		_holdoffTimer = setTimeout((function() {
			_holdoffTimer = undefined;
			if (searchBoxVal.length > 0 && searchBoxVal.trim().length > 0) {
				let reqBody = JSON.stringify({ name: searchBoxVal.trim() });
				let req = new Request("http://localhost:3001/teachers/subject", { method: "POST", body: reqBody, cache: "no-store", headers: {"Content-Type":"application/json"}});
				window.fetch(req)
					.then(res => res.json())
					.then(json => {
						this.setState({ partialName: searchBoxVal.trim(), teachers: json });
					});
			}
		}).bind(this), QUERY_HOLDOFF);
	}

}

//window.Teachers.TeachersBySubject = TeachersBySubject;
// vi: ai ts=2 sw=2
