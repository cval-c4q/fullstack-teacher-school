
const QUERY_HOLDOFF = 786; // ms
let _holdoffTimer = undefined; // timer handle

class TeachersByName extends React.Component {
	componentWillMount() {
		window.fetch("http://localhost:3001/teachers/all/", { method: "GET" })
			.then(res => res.json())
			.then(json => {
				this.setState( { teachers: json } );
			}).catch(e => alert(e.toString()));

		window.fetch("http://localhost:3001/schools", { method: "GET" })
			.then(res => res.json())
			.then(json => {
				this.setState( { schools: json } );
			}).catch(e => {console.error(e); alert(e.toString());});
	}

	render() {
		if (!(this.state && this.state.teachers && this.state.schools))
			return <h2>Fetching teacher data...</h2>;
		else if (this.state.teachers.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve teacher data from back-end server.</h2><br />
					{ "Error: " + this.state.teachers.error }<br />
					{ "Details: " + this.state.teachers.details }<br />
				</h3>
			);
		else if (this.state.schools.success === false)
			return (
				<h3 className="error">
					<h2>Failed to retrieve school data from back-end server.</h2><br />
					{ "Error: " + this.state.schools.error }<br />
					{ "Details: " + this.state.schools.details }<br />
				</h3>
			);
		else
			return (
				<div className="Namesearch-list">
					<input type="text" id="searchBox" placeholder="Start typing name/last name..."
						onChange={this.doIncrementalSearch.bind(this)}/>
					<div className="Teacher-list">
						<ul>
						{
							this.state.teachers.data.rows.filter( (teacher) => {
								let re = new RegExp("^" + this.state.partialName, "i");
								return teacher.name.split(/( |\t)+/)
									.filter(nameComponent => nameComponent.search(re) !== -1)
									.length > 0;
							}).map((matchedTeacher, idx) => {
									return <li key={idx}>{matchedTeacher.name}</li>;
							})
						}
						</ul>
					</div>
				</div>
			);
	}

	doIncrementalSearch(ev) {
		const searchBoxVal = ev.target.value;
		if (_holdoffTimer) {
			clearTimeout(_holdoffTimer);
		}
		_holdoffTimer = setTimeout((function() {
			clearTimeout(_holdoffTimer);
			if (searchBoxVal.length > 0 && searchBoxVal.trim().length > 0)
				this.setState( { partialName: searchBoxVal.trim() } );
		}).bind(this), QUERY_HOLDOFF);
	}

}

//window.Teachers.TeachersByName = TeachersByName;
// vi: ai ts=2 sw=2
