
const express = require("express");
const cJSON = require("circular-json");
const bodyParser = require("body-parser");
const { Pool } = require("pg");


///////////////////
/////
////  Setup:
//

let pool = new Pool();
let routes = new express.Router();
routes.all((req, res, next) => {
	/* All responses are in JSON  */
	res.set("Content-Type", "application/json");
	next();
});

/* Use bodyParser for all POST requests */
routes.post("*", bodyParser.json());

/**
 *   Internal helper, optionall mangles response in case of error and
 *   returns stringified result to feed back to client.
 */
function mkResponse(success, ...data) {
	let retv = {
		success,
		data: data.length === 1 ? data[0] : data,
	};

	if (!success && data.length) {
		retv.error = data[0];
		if (data.length > 1)
			retv.details = data[1];
		delete retv.data;
	}

	return cJSON.stringify(retv);
}


//////////////////
////
///  Routes:
//

/**
 *   GET /teachers/all
 */
routes.get("/teachers/all", async (req, res) => {
	let client;
	try {
		client = await pool.connect();
		const result = await client.query("SELECT * FROM skool.teachers");
		res.status(200).end(mkResponse(true, result)); 
	} catch (e) {
		res.status(500).end(mkResponse(false, "Internal server error", e.toString()));
	} finally {
		client.release();
	}
});


/**
 *   GET /schools
 */
routes.get("/schools", async(req, res) => {
	let client;
	try {
		client = await pool.connect();
		const result = await client.query("SELECT * FROM skool.schools");
		res.status(200).end(mkResponse(true, result));
	} catch (e) {
		res.status(500).end(mkResponse(false, "Internal server error", e.toString()));
	} finally {
		if (client)
			client.release();
	}
});


/**
 *   Request body format (JSON): {
 *   	name: String	// (partial) school name
 *   }
 */
routes.post("/teachers/school", async(req, res) => {
	let client;
	try {
		if (req.body.name === undefined)
			throw new Error("POST /teachers/school: JSON request body missing field 'name'.");
		client = await pool.connect();
		const result = await client.query(`SELECT skool.teachers.name as name, skool.teachers.id, skool.teachers.subject, skool.teachers.schoolid FROM skool.teachers CROSS JOIN skool.schools WHERE skool.teachers.schoolid = skool.schools.id AND UPPER(skool.schools.name) LIKE '${req.body.name.toUpperCase()}%'`);
		res.status(200).end(mkResponse(true, result));
	} catch (e) {
		res.status(500).end(mkResponse(false, "Internal server error", e.toString()));
	} finally {
		if (client)
			client.release();
	}
});


/**
 *   Request body format (JSON): {
 *   	name: String	// (partial) subject name
 *   }
 */
routes.post("/teachers/subject", async(req, res) => {
	let client;
	try {
		if (req.body.name === undefined)
			throw new Error("PST /teachers/subject: JSON request body missing field 'name'.");
		client = await pool.connect();
		const result = await client.query(`SELECT skool.teachers.name as name, skool.teachers.id, skool.teachers.subject, skool.teachers.schoolid FROM skool.teachers CROSS JOIN skool.schools WHERE skool.teachers.schoolid = skool.schools.id AND UPPER(skool.teachers.subject) LIKE '${req.body.name.toUpperCase()}%'`);
		res.status(200).end(mkResponse(true, result));
	} catch (e) {
		res.status(500).end(mkResponse(false, "Internal server error.", e.toString()));
	} finally {
		if (client)
			client.release();
	}
});


/**
 *   Fallback route
 */
routes.use((req, res) => {
	res.status(400).end(mkResponse(false, "Bad request", "Unrecognized route"));
});


module.exports = routes;
// vi: ai ts=4 sw=4
