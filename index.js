
const http = require("http");
const express = require("express");
const cors = require("cors");
const chalk = require("chalk");
const routes = require("./routes");

/***************
 *   main()
 */
const BACKEND_PORT = 3001;
const FRONTEND_PORT = 8080;

let frontServ = http.createServer(),
	backServ = http.createServer();

process.on("SIGINT", () => {
	console.log(chalk.yellow("SIGINT received. Shutting down."));
	frontServ.close();
	backServ.close();
	process.exit(0);
});

startServices();


/********************************8
*/
function startServices() {
	try {
		process.stderr.write("Back-end: bringing service up... ");
		backServ.on("error", (e) => {
			throw new Error("Back-end: server error: " + e.toString());
		});

		backServ.on("request", express().use(require("morgan")("dev"), cors(), routes));
		backServ.listen(BACKEND_PORT, () => {
			process.stderr.write(chalk.green("OK\n"));
			console.log("Back-end: service started on port", chalk.cyan(BACKEND_PORT));
			startFrontend(); // chain-start front-end
		});
	} catch (e) {
		process.stderr.write(chalk.red("FAIL\n"));
		console.error("*", e.toString());
		process.exit(255);
	}
}

function startFrontend() {
	try {
		process.stderr.write("Front-end: bringing service up... ");
		frontServ.on("error", (e) => {
			throw new Error("Front-end: server error: " + e.toString());
		});

		frontServ.on("request", express().use(express.static("static/")));
		frontServ.listen(FRONTEND_PORT, () => {
			process.stderr.write(chalk.green("OK\n"));
			console.log("Front-end: service started on port", chalk.cyan(FRONTEND_PORT));
		});
	} catch (e) {
		process.stderr.write(chalk.red("FAIL\n"));
		console.error("*", e.toString());
		process.exit(255);
	}
}

// vi: ai sw=4 ts=4
